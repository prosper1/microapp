import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import App from './App.vue'
import Home from './views/Home'
import TaskerHome from './tasker/views/Home'
import RunnerHome from './runner/views/Home'
import AyobaService from './services/AyobaService'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueSweetalert2 from 'vue-sweetalert2'

Vue.config.productionTip = false
Vue.prototype.$ayoba = AyobaService;

Vue.use(VueAxios, axios)
Vue.use(require('vue-moment'));
Vue.use(VueAwesomeSwiper)
Vue.use(VueRouter)
Vue.use(VueSweetalert2)
Vue.filter("ellipsis", function(value) {
  return value.substring(0, 12)
})
Vue.filter("mediumEllipsis", function(value) {
  return value.substring(0, 16)
})

const routes = [
  { path: '/', component: Home },
  { path: '/tasker/', component: TaskerHome },
  { path: '/runner/', component: RunnerHome }
]
const router = new VueRouter({
  routes
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
