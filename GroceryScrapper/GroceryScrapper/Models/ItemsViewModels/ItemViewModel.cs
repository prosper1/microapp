﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryScrapper.Models.ItemsViewModels
{
    public class ItemViewModel
    {
        public string Url { get; set; }
        public int ShopId { get; set; }
        public Category Category { get; set; }
    }
}
