﻿using System;
 using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GroceryScrapper.Models
{
    public class Item
    {
        [Key]
        public int Id { get; set; }

        public int ShopId { get; set; }

        public string ImageUrl { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }

        public Category Category { get; set; }

        public DateTime CretaedOn { get; set; }
    }
    public enum Category
    {
        Vegetables,
        Fruits,
        [Display(Name = "Meat & Poultry")]
        [Description("Meat & Poultry")]
        MeatAndPoultry,
        Yoghurt,
        [Display(Name = "Fresh & Smoked Fish")]
        [Description("Fresh & Smoked Fish")]
        FreshAndSmokedFish,
        Cheese,
        Eggs,
        [Display(Name = "Butter & Margarine")]
        [Description("Butter & Margarine")]
        ButterAndMargarine,
        [Display(Name = "Salads & Fresh Herbs")]
        [Description("Salads & Fresh Herbs")]
        SaladsAndFreshHerbs,
        [Display(Name = "Milk & Cream")]
        [Description("Milk & Cream")]
        MilkAndCream,
    }
}
