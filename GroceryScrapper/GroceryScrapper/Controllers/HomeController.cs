﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GroceryScrapper.Models;
using GroceryScrapper.Data;
using System.Net.Http;
using HtmlAgilityPack;
using GroceryScrapper.Models.ItemsViewModels;
using Microsoft.AspNetCore.Cors;

namespace GroceryScrapper.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [EnableCors("AnotherPolicy")]
        [Route("api/get")]
        public IActionResult GetItems()
        {
            var items = _context.Items.ToList();

            return Ok(items);
        }

        [HttpPost]
        public async Task<IActionResult> ScrapItems(ItemViewModel model)
        {
            if (ModelState.IsValid)
            {
                var url = model.Url;

                var httpClient = new HttpClient();
                var html = await httpClient.GetStringAsync(url);

                var htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(html);

                var divs = htmlDocument.DocumentNode.Descendants("div")
                    .Where(node => node.GetAttributeValue("class", "")
                    .Equals("item js-product-card-item  product-card-grid")).ToList();

                var items = new List<Item>();
                foreach (var div in divs)
                {
                    var itemName = div.Descendants("div")
                        .Where(node => node.GetAttributeValue("class", "")
                        .Equals("item-name")).FirstOrDefault().InnerText;

                    var price = div?.Descendants("div")?
                        .Where(node => node.GetAttributeValue("class", "")
                        .Equals("currentPrice  "))?.FirstOrDefault()?.InnerText;

                    var priceHasSavings = div?.Descendants("div")?
                        .Where(node => node.GetAttributeValue("class", "")
                        .Equals("currentPrice  hasSavings"))?.FirstOrDefault()?.InnerText;

                    if (price != null)
                    {
                        price = price.Replace("\r", "")
                        .Replace("\n", "")
                        .Replace("\t", "")
                        .Replace("\t", "")
                        .Replace("\t", "")
                        .Replace("R", "");

                        var convertPriceToDouble = Convert.ToDouble(price);

                        var newPrice = convertPriceToDouble / 100;

                        var imageUrl = div.Descendants("img")
                        .FirstOrDefault()
                        .ChildAttributes("src")
                        .FirstOrDefault().Value;

                        var item = new Item
                        {
                            ShopId = model.ShopId,
                            ImageUrl = imageUrl,
                            Name = itemName,
                            Price = newPrice,
                            Category = model.Category
                        };

                        _context.Add(item);
                    }
                    else if (priceHasSavings != null)
                    {
                        priceHasSavings = priceHasSavings.Replace("\r", "")
                        .Replace("\n", "")
                        .Replace("\t", "")
                        .Replace("\t", "")
                        .Replace("\t", "")
                        .Replace("R", "");

                        var convertPriceToDouble = Convert.ToDouble(priceHasSavings);

                        var newPrice = convertPriceToDouble / 100;

                        var imageUrl = div.Descendants("img")
                        .FirstOrDefault()
                        .ChildAttributes("src")
                        .FirstOrDefault().Value;

                        var item = new Item
                        {

                            ImageUrl = imageUrl,
                            Name = itemName,
                            Price = newPrice,
                            Category = model.Category,
                            CretaedOn = DateTime.Now
                        };
                        _context.Add(item);
                    }

                }
                await _context.SaveChangesAsync();

                return RedirectToAction("About", "Home");
            }
            return View("Error");
        }



        public IActionResult Products()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
