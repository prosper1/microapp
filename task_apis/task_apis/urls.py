from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework import routers
from task_runner import views
from django.conf.urls.static import static
from django.conf import settings


admin.site.site_header = "Task Runner "
admin.site.site_title = "Task Runner "


router = routers.DefaultRouter()
router.register(r'product',views.ProductViewSet)
router.register(r'profile', views.ProfileViewSet)
router.register(r'shop',views.ShopViewSet)
router.register(r'create-task',views.DoTaskViewSet)
router.register(r'accept-task',views.RunTaskViewSet)
router.register(r'tasks',views.NewTaskViewSet)
router.register(r'task-status', views.TaskStatusViewSet)
router.register(r'post-tasks',views.PostTaskViewSet)
router.register(r'Notifications', views.NotificationViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
