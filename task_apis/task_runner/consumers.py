from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from .models import Notify
import json


class NotificationConsumer(WebsocketConsumer):
    def connect(self):
        self.profile_number = self.scope['url_route']['kwargs']['id']
        print(self.scope)
        self.notification_group_name = 'notification_%s' % self.profile_number

        # Join delivery group
        async_to_sync(self.channel_layer.group_add)(
            self.notification_group_name,
            self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        # Leave delivery group
        async_to_sync(self.channel_layer.group_discard)(
            self.notification_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        # Send message to notification group
        async_to_sync(self.channel_layer.group_send)(
            self.notification_group_name,
            {
                'type': 'notification_message',
                'message': message
            }
        )

    # Receive message from delivery group
    def notification_message(self, event):
        message = event['message']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message
        }))