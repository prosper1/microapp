from django.contrib import admin
from .models import Profile, Product, Task, DoTask, Shop, Bill, ExtraData, PickProduct

# Register your models here.

admin.site.register(Product)
admin.site.register(Profile)
admin.site.register(Shop)
admin.site.register(Task)
admin.site.register(DoTask)
admin.site.register(Bill)
admin.site.register(ExtraData)
admin.site.register(PickProduct)

