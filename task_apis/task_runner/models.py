from django.db import models
from django.db.models import F, Sum
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
import asyncio
from django.db.models.signals import post_save
from django.dispatch import receiver
from channels.layers import get_channel_layer


# Create your models here.

class Profile(models.Model):
    name = models.CharField(max_length=30,blank=True)
    number = models.CharField(max_length=15,unique=True,blank=True,null=True)
    country = models.CharField(max_length=3,blank=True)
    # Change from image to test paths 
    profile_photo = models.CharField(max_length=300)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name + '--' + self.number

class Shop(models.Model):
    name = models.CharField(max_length=30)
    shop_photo = models.ImageField(upload_to='photo_folder/', default='photo_folder/None/no-img.png', verbose_name='shop-photo')
    latitude = models.CharField(max_length=30)
    longitude = models.CharField(max_length=30)
    country = models.CharField(max_length=3)
    shop_type = models.CharField(max_length=15)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

class Product(models.Model):
    name = models.CharField(max_length=2000)
    description = models.CharField(max_length=2000)
    product_photo = models.CharField(max_length=2000)
    price = models.FloatField(default=0)
    category = models.IntegerField(default=0)
    shop = models.ForeignKey(Shop, on_delete=models.SET_NULL,null=True)


    def __str__(self):
        return self.name


class Bill(models.Model):
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=200)
    price = models.IntegerField(default=0)

    def __str__(self):
        return self.name

class PickProduct(models.Model):
    product = models.ForeignKey(Product,on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)

class ExtraData(models.Model):
    products = models.ManyToManyField(Product,blank=True)
    bills = models.ManyToManyField(Bill,blank=True)
    custom = models.TextField(blank=True)

    def __str__(self):
        return str(self.products)


class Task(models.Model):

    TASK_TYPES = [
    ('SHOPPING', 'Shopping'),
    ('PAY BILLS', 'Pay Bills'),
    ('CUSTOM', 'Custom'),
    ]

    STATUS_TYPES = [
    ('PENDING', 'Pending'),
    ('ONGOING', 'ongoing'),
    ('AWAITING', 'Awaiting'),
    ('COMPLETE', 'Complete'),
    ]

    name = models.CharField(max_length=30)
    task_type = models.CharField(max_length=20, choices=TASK_TYPES,default='Shopping')
    tasker = models.ForeignKey(Profile, on_delete=models.CASCADE,related_name="task_give")
    taskee = models.ForeignKey(Profile, on_delete=models.SET_NULL,related_name="task_take",blank=True, null=True)
    status = models.CharField(max_length=15,default='Pending')
    metadata = models.ForeignKey(ExtraData, on_delete=models.SET_NULL,blank=True, null=True)
    lowest_rate = models.DecimalField(default=0,blank=True,decimal_places=2,max_digits=10)
    highest_rate = models.DecimalField(default=0,blank=True,decimal_places=2,max_digits=10)
    created = models.DateTimeField(auto_now_add=True)
    accepted = models.DateTimeField(null=True)
    modified = models.DateTimeField(null=True)


    def save(self, *args, **kwargs):

        if self.task_type in ['Shopping','SHOPPING']:
            products = self.metadata.products
            # instance.metadata.products.
            total_price = products.aggregate(
                    total_price=Sum(F('price')))
            
            self.lowest_rate = total_price.get('total_price') * 0.10
            self.highest_rate = total_price.get('total_price') * 0.25
        
        super(Task, self).save(*args, **kwargs)
    
    def __str__(self):
        return self.name


class DoTask(models.Model):
    user = models.ForeignKey(Profile, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.name + '--' + self.task.name


class RunTask(models.Model):
    user = models.ForeignKey(Profile, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.name + '--' + self.task.name

class Notify(models.Model):
    title = models.CharField(max_length=300)
    description = models.CharField(max_length=1000)
    notice_type = models.IntegerField(default=0)
    user = models.ForeignKey(Profile,related_name="notifications",on_delete=models.CASCADE)
    task = models.ForeignKey(Task,on_delete=models.CASCADE)
    is_seen = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)


class TrackTask(models.Model):
    STATUS_TYPES = [
    ('PENDING', 'Pending'),
    ('ONGOING', 'ongoing'),
    ('AWAITING', 'Awaiting'),
    ('CANCEL','Cancel'),
    ('REJECT','Reject'),
    ('COMPLETE', 'Complete'),
    ]

    requester = models.ForeignKey(Profile, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    status = models.CharField(max_length=20, choices=STATUS_TYPES, default="Ongoing")
    reason = models.CharField(max_length=200, blank=True)
    created = models.DateTimeField(auto_now_add=True)



# method for updating
@receiver(post_save, sender=RunTask, dispatch_uid="update_task")
def update_stock(sender, instance, **kwargs):
    
    if instance.task.taskee == None:
        instance.task.taskee = instance.user
        instance.task.status = 'Ongoing'
        instance.task.accepted = timezone.now()
        instance.task.modified = timezone.now()
        notification = Notify(
            notice_type=1,
            title = 'Runner Accepted Task',
            description = instance.task.taskee.name + ' has accepted to do your ' + instance.task.task_type.lower(),
            user = instance.task.tasker,
            task = instance.task
            )
        
        notification.save()

    instance.task.save()



# method for updating
# TODO dry nofify
@receiver(post_save, sender=TrackTask, dispatch_uid="update_task_status")
def update_status(sender, instance, **kwargs):

    if instance.requester == instance.task.taskee or instance.requester == instance.task.tasker:
        if instance.status in ['Cancel','Reject','REJECT','CANCEL']:
            instance.task.status = 'Ongoing'
            if instance.requester == instance.task.taskee:
                notification = Notify(
                    notice_type=1,
                    title = 'Task was ' + instance.status.lower() + "ed" ,
                    description = instance.task.tasker.name + ' has ' + instance.status.lower() + 'ed your work ' + instance.task.task_type.lower(),
                    user = instance.task.tasker,
                    task = instance.task
                )
                notification.save()

            else:
                notification = Notify(
                    notice_type=1,
                    title = 'Task was ' + instance.status.lower() + "ed" ,
                    description = instance.task.taskee.name + ' has ' + instance.status.lower() + 'ed your work ' + instance.task.task_type.lower(),
                    user = instance.task.tasker,
                    task = instance.task
                )
                notification.save()

        else:
            instance.task.status = instance.status

            if instance.status == 'AWAITING' :
                notification = Notify(
                notice_type=1,
                title = 'Task Awaiting approval!',
                description = instance.task.taskee.name + ' has requested for approval for completion of ' + instance.task.task_type.lower(),
                user = instance.task.tasker,
                task = instance.task
                )
                notification.save()

            if instance.status == 'COMPLETE' :
                notification = Notify(
                notice_type=1,
                title = 'Task Approved Complete',
                description = instance.task.tasker.name + ' has approved your task completion request of ' + instance.task.task_type.lower(),
                user = instance.task.taskee,
                task = instance.task
                )
                notification.save()

    instance.task.save()



@receiver(post_save, sender=Notify)
def notification_save_handler(sender, instance, **kwargs):
    channel_layer = get_channel_layer()
    loop = asyncio.get_event_loop()

    notify_group = 'notification_' + instance.user.number

    coroutine = async_to_sync(channel_layer.group_send)(
        notify_group,
        {
            'type': 'send_message',
            'update': instance,
        }
    )
    loop.run_until_complete(coroutine)