from rest_framework import serializers
from rest_framework.relations import HyperlinkedRelatedField
from .models import (
    Profile,
    Product,
    Task,
    DoTask,
    Shop,
    RunTask,
    ExtraData,
    Bill,
    PickProduct,
    TrackTask,
    Notify
)
from drf_writable_nested.serializers import  WritableNestedModelSerializer


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = (
            'id',
            'name',
            'description',
            'shop',
            'price',
            'category',
            'product_photo'
            )


class DoTaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = DoTask
        fields = ('user','task')


class RunTaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = RunTask
        fields = ('id','user','task')


class TaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        fields = ('id','name')


class ShopSerializer(serializers.ModelSerializer):

    class Meta:
        model = Shop
        fields = ('id','name','shop_photo','latitude','longitude')



class ProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = Profile
        fields = ('id','name','number','country','profile_photo')
        extra_kwargs = {
            'name': {'required': False},
            'country': {'required': False},
            }

class BillSerializer(serializers.ModelSerializer):

    class Meta:
        model = Bill
        fields = ('name','description','price')


class ExtraDataSerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True, read_only=False)
    bills = BillSerializer(many=True, read_only=False)

    class Meta:
        model = ExtraData
        fields = ('products','bills','custom')
        extra_kwargs = {
            'products': {'required': False},
            'bills': {'required': False},
            'custom': {'required': False},
            }


class NewTaskSerializer(serializers.ModelSerializer):
    metadata = ExtraDataSerializer(many=False, read_only=False)
    tasker = ProfileSerializer(many=False, read_only=False)
    taskee = ProfileSerializer(many=False, read_only=False)

    class Meta:
        model = Task
        fields = (
            'id',
            'tasker',
            'taskee',
            'name',
            'task_type',
            'status',
            'metadata',
            'lowest_rate',
            'highest_rate',
            'created',
            'accepted',
            'modified'
            )
        read_only_fields = ('modified','accepted')
        extra_kwargs = {
            'taskee': {'required': False},
            'metadata': {'required': False},
            }

class PickProductSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = PickProduct
        fields = ('product','quantity')
        
        

class PostExtraDataSerializer(WritableNestedModelSerializer):

    class Meta:
        model = ExtraData
        fields = ('products','bills','custom')
        

        
class PostTaskSerializer(WritableNestedModelSerializer):
    metadata = PostExtraDataSerializer(many=False, read_only=False)

    class Meta:
        model = Task
        fields = (
            'id',
            'tasker',
            'taskee',
            'name',
            'task_type',
            'status',
            'metadata',
            'lowest_rate',
            'highest_rate',
            'created',
            'accepted',
            'modified',

            )
        read_only_fields = ('modified','accepted')


class TaskStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = TrackTask
        fields = (
            'id',
            'requester',
            'task',
            'status',
            'reason',
            'created'
            )


class NotificationsSerializer(serializers.ModelSerializer):
    task = NewTaskSerializer(many=False,read_only=True)

    class Meta:
        model = Notify
        fields = (
            'user',
            'title',
            'description',
            'notice_type',
            'is_seen',
            'task',
            'created'
        )