from django.shortcuts import render
from .models import (
   Product,
   Profile,
   DoTask,
   RunTask,
   Shop,
   Task,
   Bill,
   ExtraData,
   TrackTask,
   Notify
)
from django.contrib.auth.models import User
from .serializers import (
    ProductSerializer,
    ProfileSerializer,
    RunTaskSerializer,
    DoTaskSerializer,
    ShopSerializer,
    TaskSerializer,
    NewTaskSerializer,
    PostTaskSerializer,
    TaskStatusSerializer,
    NotificationsSerializer
)

from rest_framework import viewsets , serializers
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from django.contrib.auth.models import User
from rest_framework.renderers import JSONRenderer

from django.views.generic import View
from rest_framework.decorators import action
from rest_framework.response import Response


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('price','shop')

class ProfileViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('number','id')

class ShopViewSet(viewsets.ModelViewSet):
    queryset = Shop.objects.all()
    serializer_class = ShopSerializer
 
class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

class DoTaskViewSet(viewsets.ModelViewSet):
    queryset = DoTask.objects.all()
    serializer_class = DoTaskSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('user','task')

class RunTaskViewSet(viewsets.ModelViewSet):
    queryset = RunTask.objects.all()
    serializer_class = RunTaskSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('user','task',)

    @action(detail=True, methods=['post'])
    def do_accept(self, request, pk=None):
        run_task = self.get_object()
        print(request.data)
        serializer = RunTaskSerializer(data=request.data)
        if serializer.is_valid():
            run_task.do_accept(serializer.data['task','user'])
            print(serializer.data['task','user'])
            task = Task.objects.all()
            run_task.save()
            return Response({'status': 'task accepted'})
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

        
class NewTaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = NewTaskSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('tasker','taskee','status')

class PostTaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = PostTaskSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('tasker','taskee','status')

class TaskStatusViewSet(viewsets.ModelViewSet):
    queryset = TrackTask.objects.all()
    serializer_class = TaskStatusSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('task','requester','status',)


class NotificationViewSet(viewsets.ModelViewSet):
    queryset = Notify.objects.all()
    serializer_class = NotificationsSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('user','is_seen',)

