# Generated by Django 2.2.10 on 2020-04-04 13:31

import datetime
from django.db import migrations, models
from django.utils.timezone import utc
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('task_runner', '0007_auto_20200404_1307'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='created',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='task',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime(2020, 4, 4, 13, 31, 32, 131988, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
