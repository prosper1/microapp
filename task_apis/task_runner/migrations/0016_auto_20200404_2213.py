# Generated by Django 2.2.10 on 2020-04-04 22:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('task_runner', '0015_auto_20200404_2123'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='modified',
            field=models.DateTimeField(null=True),
        ),
    ]
