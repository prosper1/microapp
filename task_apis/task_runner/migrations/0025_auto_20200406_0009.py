# Generated by Django 2.2.10 on 2020-04-05 22:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('task_runner', '0024_notify_is_seen'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='profile_photo',
            field=models.CharField(max_length=300),
        ),
    ]
